package com.alexandergolovinov.climapm;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class WeatherDataModel {

    private String mTemperature;
    private int mCondition;
    private String mCity;
    private String mIconName;

    private final static double KELVIN_CONSTANT = 273.15;

    public static WeatherDataModel getDataFromJson(JSONObject jsonObject) {
        WeatherDataModel weatherData = new WeatherDataModel();
        try {
            weatherData.setCity(jsonObject.getString("name"));
            weatherData.setCondition(jsonObject.getJSONArray("weather").getJSONObject(0).getInt("id"));
            weatherData.setIconName(updateWeatherIcon(weatherData.getCondition()));

            double tempKelvin = jsonObject.getJSONObject("main").getDouble("temp");
            weatherData.setTemperature(convertKelvinToCelcius(tempKelvin));


        } catch (JSONException e) {
            Log.e("Clima", e.toString());
        }
        return weatherData;
    }

    private static String convertKelvinToCelcius(final double kelvin) {
        return Integer.toString((int) Math.rint(kelvin - KELVIN_CONSTANT));
    }


    private static String updateWeatherIcon(int condition) {

        if (condition >= 0 && condition < 300) {
            return "tstorm1";
        } else if (condition >= 300 && condition < 500) {
            return "light_rain";
        } else if (condition >= 500 && condition < 600) {
            return "shower3";
        } else if (condition >= 600 && condition <= 700) {
            return "snow4";
        } else if (condition >= 701 && condition <= 771) {
            return "fog";
        } else if (condition >= 772 && condition < 800) {
            return "tstorm3";
        } else if (condition == 800) {
            return "sunny";
        } else if (condition >= 801 && condition <= 804) {
            return "cloudy2";
        } else if (condition >= 900 && condition <= 902) {
            return "tstorm3";
        } else if (condition == 903) {
            return "snow5";
        } else if (condition == 904) {
            return "sunny";
        } else if (condition >= 905 && condition <= 1000) {
            return "tstorm3";
        }

        return "dunno";
    }

    public int getCondition() {
        return mCondition;
    }


    public void setCondition(int condition) {
        mCondition = condition;
    }

    public String getTemperature() {
        return mTemperature + "°";
    }

    public String getCity() {
        return mCity;
    }

    public String getIconName() {
        return mIconName;
    }

    public void setTemperature(String temperature) {
        mTemperature = temperature;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public void setIconName(String iconName) {
        mIconName = iconName;
    }
}
